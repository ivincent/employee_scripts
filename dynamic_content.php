<?php
/*
=======================================Introduction Begins===========================================================
***This file entails result-set that are managed in terms of add/view/modify/update actions
***Therefore $click will store a specific action on a specific result-set
***For instance to add records to an employees table - $click will be associated with addEmployees
***The following are the result-sets being managed at the moment:
Employees



***Other $click variables that are NOT managed from this file:
login [form: accessed from the home page - originates from the index.php file]
logout [form: accessed from any page after logging in - originates from template.php file]


***You should remember that besides $click the other commonly used variables
***are documented in the config.php file. At the moment the common variables include:
$click---Variable stores a unique value for a currently clicked hyperlink or form
$formName---variable stores the name of a form that has currently submitted updated data.
$modify---variable stores Update/Delete action buttons from a form that has currently submitted data
$add---variable stores add action button from a form that has currently submitted new data
=======================================Introduction Ends=============================================================
*/





/*
=======================================Managing Employees Record-set Begins==========================================
*** Section for managing Employees Records
***The $click variables for managing employees:
addEmployees [link: accessed from "Add Employees" Main menu]
modifyEmployees [form: accessed through "Add Employees" main menu]
updateEmployees [form: accessed through "Add Employees" main menu => then after checking selected employees for update]
viewEmployees[link: accessed from "View Employees" Main menu]
*/


//===Generating a lookup table for existing departments in cities.
$get_dids =mysql_query("SELECT did,dname,city FROM dept inner join loc on (dept.lid = loc.lid)");
$lookup_dept = '';
while($did_row = mysql_fetch_array($get_dids)){
	$did = $did_row['did'];
	$dname = $did_row['dname'];
	$city = $did_row['city'];
	$lookup_dept .= "<option value='$did'>$dname in $city</option>";
}


//===Arrays used on selected employees
$eid = $_REQUEST['eid'];
$fname = $_REQUEST['fname'];
$dob = $_REQUEST['dob'];
$sal = $_REQUEST['sal'];
$did = $_REQUEST['did'];


if ($click == 'viewEmployees' or $click == 'modifyEmployees' or $click == 'updateEmployees'){

	if($click == 'updateEmployees'){
		while(list($key,$value) = each($eid)){
			$eid_value = $value;
			$fname_value = $fname["$key"];
			$did_value = $did["$key"];
			$sal_value = $sal["$key"];
			$dob_value = $dob["$key"];
			mysql_query("update emp set fname = '$fname_value', dob = '$dob_value', sal = '$sal_value', did = '$did_value' where eid = '$eid_value'");
		}
	}
	

	if($click == 'modifyEmployees'){
		if((isset($modify) and isset($eid))){
			$eid_list = '';
			while(list($key,$value) = each($eid)){
				$eid_list .= "'$value',";
			}
			$eid_list = trim($eid_list, ' ,');
			if ($modify == 'Delete'){
				mysql_query ("delete from emp where eid in ($eid_list)") or die (mysql_error());
				$feedback = "<p><font color='blue'><b>Previously</b></font> checked $formName deleted successfully";
			}else{
//--------To be well aligned
				

//===Generating update form

	$sql= "SELECT eid,dept.did,loc.lid,fname, dob, sal, dname, city
FROM emp
LEFT OUTER JOIN dept ON ( emp.did = dept.did ) 
LEFT OUTER JOIN loc ON ( dept.lid = loc.lid ) where eid in ($eid_list)";

	$get_rows = mysql_query($sql) or die (mysql_error());
	if (mysql_num_rows($get_rows) > 0){	
		$dynamic_content = "<p>Updating $formName<form action='?click=updateEmployees' method='POST' name='Employees'><input type='hidden' name='formName' value='Employees'>
		<table border=1><tr><th>Employee ID</th> <th>First Name</th> <th>Date of Birth</th><th>Salary</th><th>Department in City</th></tr>";
		while($row = mysql_fetch_array($get_rows)){
			$eid = $row["eid"];
 			$did = $row["did"];
 			$fname = $row["fname"]; 
			$dob = $row["dob"];
			$sal = $row["sal"];
			$dname = $row["dname"];
			$city = $row["city"];
			$dynamic_content .= "<tr>
						<td>$eid<input type='hidden' name='eid[]' value='$eid'></td>
						<td><input type='text' name='fname[]' value='$fname'></td> 
						<td><input type='text' name='dob[]' value='$dob'></td>
						<td><input type='text' name='sal[]' value='$sal'></td>
						<td><select name='did[]'>
							<option value='$did'>$dname in $city</option>
							$lookup_dept
							</select>
						</td>
					    </tr>";
		}
		$dynamic_content .= "<tr>
					<td colspan='5' align='center'>
					<input type='submit' name='modify' value = 'Update'> 
					</td>
				    </tr>
				</table></form>";

					//$feedback = $dynamic_content;
	}

//--------

			}
		}else{
			$feedback = "<p><font color='red'><b>Note:</b></font> ensure that you have checked the $formName for '<font color='red'><b>$modify</b></font>'";
		}
	}


	
	if ($click == 'viewEmployees' or $click == 'updateEmployees' or isset($feedback)){

	$sql= "SELECT eid,fname, dob, sal, dname, city
FROM emp
LEFT OUTER JOIN dept ON ( emp.did = dept.did ) 
LEFT OUTER JOIN loc ON ( dept.lid = loc.lid )";

	$get_rows = mysql_query($sql) or die (mysql_error());
	if (mysql_num_rows($get_rows) > 0){
		$dynamic_content = "<p>All Employees<form action='?click=modifyEmployees' method='POST' name='Employees'><input type='hidden' name='formName' value='Employees'>
		<table border=1><tr><th>Employee ID</th><th>First Name</th> <th>Date of Birth</th><th>Salary</th><th>Department</th><th>Dept. Location</th><th>Modify</th></tr>";
		while($row = mysql_fetch_array($get_rows)){
			$eid = $row["eid"];
 			$fname = $row["fname"]; 
			$dob = $row["dob"];
			$sal = $row["sal"];
			$dname = $row["dname"];
			$city = $row["city"];
			$dynamic_content .= "<tr><td>$eid</td><td>$fname</td> <td>$dob</td><td>$sal</td><td>$dname</td><td>$city</td>
						<td align='center'><input name='eid[]' type ='checkbox' value = '$eid'></td></tr>";
		}
		$dynamic_content .= "<tr>
					<td colspan='6'> </td>
					<td><input type='submit' name='modify' value = 'Update'>:::<input type='submit' name='modify' value = 'Delete'> </td>
				    </tr>
				</table></form>";
		$dynamic_content = "$feedback $dynamic_content";




	}else{
		$dynamic_content = "COMPANY has no Employees";
	}
	}//---end of "if ($click == 'viewEmployees' or $click == 'updateEmployees' or isset($feedback)){"






}


//

if ($click == 'addEmployees' or (isset($add) and $click == 'addEmployees')){
	if(isset($add)){
		$successful_additions = '';
		while(list($key,$value) = each($eid)){
			$eid_value = $value;
			$fname_value = $fname["$key"];
			$did_value = $did["$key"];
			$sal_value = $sal["$key"];
			$dob_value = $dob["$key"];

			$eid_value = trim($eid_value);
			$fname_value = trim($fname_value);
			$did_value = trim($did_value);
			$sal_value = trim($sal_value);
			$dob_value = trim($dob_value);
			
			if (empty($eid_value) or empty($fname_value)){
				continue;
			}
			$fname_value = addslashes($fname_value);
			$successful_additions .= "$eid_value $fname_value; ";
			
			$insert = mysql_query("insert into emp(fname,dob,sal,did,eid) values('$fname_value','$dob_value','$sal_value','$did_value','$eid_value')");
		}
		$successful_additions = trim ($successful_additions, ' ;');
		if ($insert){
			$feedback = "<p><font color='blue'><b>Successfully</b></font> added $formName records: $successful_additions.";
		}else{
			$feedback = "<p><font color='red'><b>Note:</b></font> No $formName records were added.";
		}
	}

	


			$dynamic_content = "<p>Adding Employees<form action='?click=addEmployees' method='POST' name='Employees'><input type='hidden' name='formName' value='Employees'>
		<table border=1><tr><th>Employee ID</th> <th>First Name</th> <th>Date of Birth</th><th>Salary</th><th>Department in City</th></tr>";
		$rows_displayed = 5;
		$row = 1;
		while($row <= $rows_displayed){
			$dynamic_content .= "<tr>
						<td><input type='text' name='eid[]'></td>
						<td><input type='text' name='fname[]'></td> 
						<td><input type='text' name='dob[]'></td>
						<td><input type='text' name='sal[]'></td>
						<td><select name='did[]'>$lookup_dept</select></td>
					    </tr>";
			$row++;
		}
		$dynamic_content .= "<tr>
					<td colspan='5' align='center'>
					<input type='submit' name='add' value = 'Add'> 
					</td>
				    </tr>
				</table></form>";

	if(isset($add)){
		$dynamic_content = "$feedback $dynamic_content";
	}


}
//=======================================Managing Employees Record-set Ends============================================

//Display any content for confirmation of unknow output from here
//echo "<p>Jesus is the answer";

?>