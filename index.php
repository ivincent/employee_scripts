<?php
/*
***Index page contains 3 major parts:
*/

//===Part 1: General setup of the website application stored in the 'config.php' file
include('config.php');


//===Part 2: Login page accessed the first time the website is accessed or after logging out
if (is_null($click) or $click == 'logout'){
	if($click == 'logout'){
		session_start();
		session_unset();
		session_destroy();
	}

	echo "<html>
		<head>
		<title>$page_title</title>
		<!-- CSS plus dynamic html -->
		</head>

		<body>
		<br><br><br><br><br>
		<center>
		<form method='POST' action='?click=login'><input type='hidden' name='formName' value='Login'>
			NAME: <input type ='text' name = 'username'><br>
			<input type ='submit' name='add' value='Login'>
		</form>
		</center>
		</body>
	<html>";
	exit;
}



//===Part 3: Display of other pages accessed beyond the Login page
//Note that the template variables come before the template.
if (isset($click) and $click != 'logout'){

	//3.1 Session started after login to determine user's personal details
	//This is should be the first part because it can affect the choice
	//of the other parts from 3.2 to 3.7
	session_start();
	if ($click == 'login'){
		$username = $_REQUEST['username'];
		$username = trim($username);

		if ($username){
			$_SESSION['username'] = $username;
		}else{
			$_SESSION['username'] = 'GUEST';
		}
	}
	$username = $_SESSION['username'];
	if (empty($username)){
		echo "You are not logged in. <meta HTTP-equiv=\"refresh\"content=\"1;url=index.php\">";
		exit;
	}

	//3.2 Style file (CSS/DHTML/XML/Ajax) included
	//include('style.php');


	//3.3 Title page changes based on accessed page
	$page_title = $_REQUEST['click'];
	if ($click == 'login'){
		$page_title = "$company_name";
	}



	//3.4 Logo image with motto/vision
	//$logo = '/path_to_selected_logo';


	//3.5 Navigation can have two menus (main menu and sub menu)
	//$main_menu;
	//$sub_menu;


	//3.6 Dynamic content displayed based on clicks made from forms and hyperlinks
	include('dynamic_content.php');


	//3.7 template used
	//This should be the last part since it contains variables for parts 3.1 to 3.6
	include('template.php');
	exit;
}

?>