<?php
/*
*** Change the following 5 variables appropriately
*/
//===1.Change the company name to an appropriate value
$company_name = "Uganda Healthcare System";


//===2.Speacify the name of the database to be created
$database = 'employees';


//===3.Specify the username of an account that has access to the database
$db_username = 'root';


//===4.Specify the password of for the database of the username account
$db_password = '';


//===5.specify the IP address of the machine hosting the database
$db_host_machine = 'localhost';




/*
***Automatically Setting up the database based on the above parameters
*/
$machine_connection = mysql_connect($db_host_machine, $db_username, $db_password) or die(mysql_error());
mysql_select_db($database,$machine_connection) or die(mysql_error());



$create_emp = "CREATE TABLE emp (eid varchar(5) NOT NULL,fname varchar(30) NOT NULL,dob date, sal int(11), did char(2), PRIMARY KEY (eid) )";
$insert_emp ="INSERT INTO emp(eid, fname, dob, sal, did) VALUES
('e1', 'jack', '1987-02-23', 1000000, 'd3'),
('e2', 'jill', '1977-02-16', 1500000, null),
('e3', 'sarah', '1987-12-01', null, 'd2'),
('e4', 'peter', '1990-10-06', 2000000, 'd1'),
('e5', 'jane', '1986-01-26', 1500000, 'd2')";

$create_dept ="CREATE TABLE dept (did char(2) NOT NULL, dname varchar(25),lid char(2),PRIMARY KEY (did))";
$insert_dept = "INSERT INTO dept (did, dname, lid) VALUES ('d1', 'sales', 'L1'),
('d2', 'purch', 'L2'), ('d3', 'mkt', null), ('d4', 'IT', 'L3')";

$create_loc ="CREATE TABLE loc (lid char(2) NOT NULL, city varchar(10), PRIMARY KEY (lid))";
$insert_loc ="INSERT INTO loc(lid, city) VALUES ('L1', 'Kampala'), ('L2', 'Entebbe'), ('L3', 'Gulu')";
/*
mysql_query($create_emp);
mysql_query($insert_emp);
mysql_query($create_dept);
mysql_query($insert_dept);
mysql_query($create_loc);
mysql_query($insert_loc);
*/


/*
*** Variables commonly used throughout the application
*/
$click = $_REQUEST['click']; //---Variable stores a unique value for a currently clicked hyperlink or form
$formName = $_REQUEST['formName']; //---variable stores the name of a form that has currently submitted updated data.
$modify = $_REQUEST['modify']; //---variable stores Update/Delete action buttons from a form that has currently submitted data
$add = $_REQUEST['add']; //---variable stores add action button from a form that has currently submitted new data

//Disabling brower cache
header("Cache-control: private");


?>